package com.executionscenarios.testconfigs;

import cucumber.api.java.it.Ma;
import org.awaitility.core.ConditionTimeoutException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.given;

public class DbUtils {
    private static final Logger logger = LoggerFactory.getLogger(DbUtils.class);

    private final JdbcTemplate jdbc;

//    public DbUtils(String connectionUrl) {
//        String url = (String) connectionUrl;
//        String driver = (String) "oracle.jdbc.driver.OracleDriver";
//        DriverManagerDataSource dataSource = new DriverManagerDataSource();
//        dataSource.setDriverClassName(driver);
//        dataSource.setUrl(url);
//        jdbc = new JdbcTemplate(dataSource);
//        logger.info("Initialize the JDBC template: {}", url);
//    }

    public DbUtils(Map<String, Object> config) {
        String userName = (String) config.get("uname");
        String password = (String) config.get("pwd");
        String driver = (String) "oracle.jdbc.driver.OracleDriver";
        String connecUrl = (String) config.get("url");

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setUsername(userName);
        dataSource.setPassword(password);
        dataSource.setDriverClassName(driver);
        dataSource.setUrl(connecUrl);

        jdbc = new JdbcTemplate(dataSource);
        logger.info("Initialize the JDBC template: {}", connecUrl);
    }

    public Object readValue(String query) {
        try {
            given().pollDelay(1, TimeUnit.SECONDS).pollInterval(10, TimeUnit.SECONDS).await().atMost(60, TimeUnit.SECONDS).until(
                    () -> {
                        return jdbc.queryForRowSet(query).isBeforeFirst();
                    }
            );
            return jdbc.queryForObject(query, Object.class);
        } catch (ConditionTimeoutException e) {
            logger.info("Database Query for a value did not return any results, and the following error occurred : " + e.getMessage());
            throw e;
        }
    }

    public Map<String, Object> readRow(String query) {
        try {
            given().pollDelay(1, TimeUnit.SECONDS).pollInterval(10, TimeUnit.SECONDS).await().atMost(60, TimeUnit.SECONDS).until(
                    () -> {
                        return jdbc.queryForRowSet(query).isBeforeFirst();
                    }
            );
            return jdbc.queryForMap(query);
        } catch (ConditionTimeoutException e) {
            logger.info("Database Query for a row did not return any results, and the following error occurred : " + e.getMessage());
            throw e;
        }
    }

    public List<Map<String, Object>> readRows(String query) {
        try {
            given().pollDelay(1, TimeUnit.SECONDS).pollInterval(10, TimeUnit.SECONDS).await().atMost(60, TimeUnit.SECONDS).until(
                    () -> {
                        return jdbc.queryForRowSet(query).isBeforeFirst();
                    }
            );
            return jdbc.queryForList(query);
        } catch (ConditionTimeoutException e) {
            logger.info("Database Query for a list of rows did not return any results, and the following error occurred : " + e.getMessage());
            throw e;
        }
    }
}