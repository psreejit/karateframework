@ignore
Feature: A sample test to validate random key value combinations from a section of a response

  Background:
    * def endPointUrl = caponeurl
    * def baseJsonRequest = read('../requesttemplates/request3capone.json')
    * configure headers = read('../testauthorizations/caponeheaders.json')

  Scenario: Json elements with limited number of steps
    Given url endPointUrl
    And  header Content-Type = 'application/vnd.com.equifax.ic.saas.request.v1+json'
    And request baseJsonRequest
    When method post
    Then status 202
    * def efxUSConsCrdtRpt = $.applicants.primaryConsumer.equifaxUSConsumerCreditReport.equifaxUSConsumerCreditReport
    * print efxUSConsCrdtRpt
    And match efxUSConsCrdtRpt contains {"consumerReferralCode": "198", "identityScanIndicatorCode": "I","identityScanAlerts": [{ "code": "#string", "description": "#string"}]}
    * def actualCodes = $..code
    * print actualCodes
    And match actualCodes contains only ['S','H']