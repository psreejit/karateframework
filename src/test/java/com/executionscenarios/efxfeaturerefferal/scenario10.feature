@ignore
Feature: Execute a DB validation with an encrypted password.

  Background:
    * def DbUtils = Java.type(dbUtilsClass)
    * def dbConfig = {uname:'test', pwd:'test',url:'jdbc:oracle:thin:@//clu-dx-testprod.com:1532/RAJ_DAL_APP'}
    * def db = new DbUtils(dbConfig)

  Scenario: SC-1_Executing a query, converting result as JSON, looping & validating eventypeid has only numbers
    * def test = db.readValue("select eventtypeid from systemevent where TRANSACTION_ID=110061000003658601 order by EVENTTYPE_NAME")
    * print test

    * def test2 = db.readRow("select * from systemevent where TRANSACTION_ID=110061000003658601 order by EVENTTYPE_NAME")
    * print test2

    * def test3 = db.readRows("select CORRELATION_ID,EVENTTYPEID,ORGANIZATION_CODE,ORCHESTRATION_CODE,EVENT_STATUS,EVENT_DURATION,EVENTTYPE_NAME,RESPONSE_STATUS,EVENTTYPE,SYSTEMID,BUSINESS_ORCHESTRATION_CODE,COMMUNICATION_TIME,CONSUMER_HOST_NAME,EVENT_MESSAGE,ERROR_MESSAGE from systemevent where CORRELATION_ID = '591fd8a0-e943-4e6e-ba78-8ee255ab8568' order by EVENTTYPE_NAME")
    * print test3