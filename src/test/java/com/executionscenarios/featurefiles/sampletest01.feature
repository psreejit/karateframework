Feature: A sample web service test

  Background:
    * def endpointUrl = endPointURI + path

  Scenario: My First Sample Scenario
    Given url endpointUrl
    When method get
    Then status 200

  Scenario: My Second Example
    Given url 'https://jsonplaceholder.typicode.com/users'
    When method get
    Then status 200