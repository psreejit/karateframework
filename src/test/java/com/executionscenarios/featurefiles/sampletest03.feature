@ignore
Feature: A sample web service test

  Background:
    * def endpointUrl = endPointURI + path

  Scenario: My First Sample Scenario
    #Given url endpointUrl
    #When method get
    #Then status 200

    # use jdbc to validate
    * def config = { username: 'hr', password: 'hr', url: 'jdbc:oracle:thin:@localhost:1521:xe', driverClassName: 'oracle.jdbc.driver.OracleDriver' }
    * def DbUtils = Java.type('com.executionscenarios.testconfigs.DbUtils')
    * def db = new DbUtils(config)
    * def test = db.readRows('select * from COUNTRIES')
    * print test