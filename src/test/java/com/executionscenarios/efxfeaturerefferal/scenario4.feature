@ignore
Feature: A sample feature of Capone saas. It is just to pull ACRO credit report.
  The request is for Frozen file returned. Header authorization is also done in this test

  Background:
    * def endPointUrl = caponeurl
    * def baseJsonRequest = read('../requesttemplates/request3capone.json')
    * def baseData = read('../testdata/sampledata.json')
    # * header Authorization = call read('../testauthorizations/basic-auth.js') { username: 'testuser', password: 'password1' }
    * configure headers = read('../testauthorizations/caponeheaders.json')

  Scenario: Capone saas pulls ACRO credit report, prints a value to a config properties file. Custom Java, Javascript Inclusions
    * def doStorage =
          """
          function (args){
              var DataStorage = Java.type('com.demotestscenarios.testconfigs.DataStorage');
              var ds = new DataStorage();
              return ds.write(args);
          }
          """
    Given url endPointUrl
    And  header Content-Type = 'application/vnd.com.equifax.ic.saas.request.v1+json'
    And request baseJsonRequest
    When method post
    Then status 202
    * print "The Response is: ", response
    * def efxUSConCrdRpr = $.applicants.primaryConsumer.equifaxUSConsumerCreditReport.equifaxUSConsumerCreditReport
    * print efxUSConCrdRpr

    # Executing few more validations
    And match efxUSConCrdRpr.identityScanAlerts == [{ "code": "S", "description": "Identity Scan did not detect any alerts."}]
    And match efxUSConCrdRpr.hitCode == 'H'
    And match efxUSConCrdRpr.regulatoryAlerts == [{"code":"#regex [A-Z]{1}","message":"#string","type":"#string"}]

    # Taking the data from outside and doing a fuzzy matching
    * print baseData.testData.Scenario4.OFACAlerts
    * print efxUSConCrdRpr.OFACAlerts
    And match efxUSConCrdRpr.OFACAlerts == baseData.testData.Scenario4.OFACAlerts
    * def legalVerbiage = efxUSConCrdRpr.OFACAlerts[0].legalVerbiage
    * print "Legal verbiages is: ", legalVerbiage
    * call doStorage {'key': #(legalVerbiage)}

  Scenario: Call a value which has been written to the property file from the previous script. Custom Java, Javascript Inclusions
    * def readFromProperties =
          """
          function (args){
            var DataStorage = Java.type('com.demotestscenarios.testconfigs.DataStorage');
            var ds = new DataStorage();
            return ds.read(args);
          }
          """
    Given url endPointUrl
    And  header Content-Type = 'application/vnd.com.equifax.ic.saas.request.v1+json'
    And request baseJsonRequest
    When method post
    Then status 202
    * def result = call readFromProperties 'sampleKey'
    * print result