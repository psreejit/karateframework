@ignore
Feature: A sample test to evaluate a section of a json in a single step by calling a test json subset to be
  validated from another test data file

  Background:
    * def endPointUrl = caponeurl
    * def baseJsonRequest = read('../requesttemplates/request3capone.json')
    * def baseData = read('../testdata/sampledata.json')
    * configure headers = read('../testauthorizations/caponeheaders.json')

  Scenario: A scenario to execute multiple requests using different PII's and validate the response status
    Given url endPointUrl
    And  header Content-Type = 'application/vnd.com.equifax.ic.saas.request.v1+json'
    And request baseJsonRequest
    When method post
    Then status 202
    * def actualResponse = $.applicants.primaryConsumer.equifaxUSConsumerCreditReport.equifaxUSConsumerCreditReport
    * def expectedTestData = baseData.testData.Scenario7.equifaxUSConsumerCreditReport
    * print actualResponse
    * print expectedTestData

    And match expectedTestData == actualResponse