@ignore
Feature: A Sample feature to illustrate JSON webservice test

  Background:
    * def baseJsonRequest = read('../requesttemplates/request1.json')
    * def endPointUrl = endPointURI + path

  @regression
  Scenario: SC-1_My Sample Scenario - Calling a request directly and validating the responses
    Given url endPointUrl
    And request baseJsonRequest
    When method post
    Then status 200
    * print response
    * def autoResponseProducts = $.autoResponse.applicationHeader.products
    And match autoResponseProducts == [{"action": "Submit","code": "JDB","name": "JDB Submit"}]

  @regression
  Scenario: SC-2_My Sample Scenario - Validate just a part in json object
    Given url endPointUrl
    And request baseJsonRequest
    When method post
    Then status 200
    * def autoResponseProducts = $.autoResponse.applicationHeader.products
    * print autoResponseProducts
    * def part = { code:JDB }
    And match autoResponseProducts contains '#(^part)'

  @regression
  Scenario: SC-3_My Sample Scenario - Validate a response json array and evaluate all
  the response values are not Null
    Given url endPointUrl
    And request baseJsonRequest
    When method post
    Then status 200
    * def mitScores = $.autoResponse.applicants[*].applicant.specifiedPerson.customModel[0].reasonsForScore[*].code
    * print mitScores
    And match each mitScores != 'null'


  Scenario: SC-4_My sample Scenario - Looping through a JSON array and checking the following.
  For a key the value should be 2 digits, only numbers and length of array should be 4
    * def mySampleData = [{"code":39,"description":"Serious delinquency"},{"code":40,"description":"Proportion of balances to credit limits is too high on bank revolving or other revolving accounts"},{"code":41,"description":"Amount owed on revolving accounts is too high"},{"code":42,"description":"Too many consumer finance company accounts"}]
    * print mySampleData
    * def min = 38
    * def max = 50
    And match each mySampleData contains only {code:'#? _ >= min && _ <= max', description:"#ignore"}
    * print 'The length of the array should be 4'
    And match mySampleData == '#[4]'