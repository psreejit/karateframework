function configurations() {
    var env = karate.env; // get system property 'karate.env'
    karate.log('Env is being set from maven as:', env);

    karate.configure('connectTimeout', 50000);
    karate.configure('readTimeout', 50000);

    switch (env) {
        case 'QA':
            var config = {
                environment: 'The environment is set as QA',
                endPointURI: '',
                path: ''
            };
            break;

        default:
            var configs = karate.callSingle('classpath:com/executionscenarios/featurefilesreusable/getcmdbvalues.feature');
            karate.log('******Configs Enpoint Uri values are ******:', configs.enpointURI)
            var config = {
                             environment: 'Since the env from maven is null, the default environment is set',
                             endPointURI: configs.enpointURI,
                             path: '/api/74e36bb7a98f6a55/conditions/q/CA/San_Francisco.json'
                         };
            karate.log('******Pass Config values are ******:', config)

    }
    karate.log("Environment : ", config.environment);
    karate.log("End point URI : ", config.endPointURI);
    karate.log("Path : ", config.path);
    return config;
}