@ignore
Feature: Execute a DB validation using this framework. Try Options to take a value, take a row and take multiple rows

  Background:
    * def DbUtils = Java.type(dbUtilsClass)
    * def db = new DbUtils(dbConfigUrl)

  Scenario: SC-1_Executing a query, converting result as JSON, looping & validating eventypeid has only numbers
    * def test = db.readValue("select eventtypeid from systemevent where TRANSACTION_ID=110061000003658601 order by EVENTTYPE_NAME")
    * print test

    * def test2 = db.readRow("select * from systemevent where TRANSACTION_ID=110061000003658601 order by EVENTTYPE_NAME")
    * print test2

    * def test3 = db.readRows("select CORRELATION_ID,EVENTTYPEID,ORGANIZATION_CODE,ORCHESTRATION_CODE,EVENT_STATUS,EVENT_DURATION,EVENTTYPE_NAME,RESPONSE_STATUS,EVENTTYPE,SYSTEMID,BUSINESS_ORCHESTRATION_CODE,COMMUNICATION_TIME,CONSUMER_HOST_NAME,EVENT_MESSAGE,ERROR_MESSAGE from systemevent where CORRELATION_ID = '591fd8a0-e943-4e6e-ba78-8ee255ab8568' order by EVENTTYPE_NAME")
    * print test3