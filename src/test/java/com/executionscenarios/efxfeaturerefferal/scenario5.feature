@ignore
Feature: A reusable feature file to do a loop execution. The user names will be updated for each request.
  User name will taken from usernames.json

  Background:
    * def baseData = read('../testdata/usernames.json')
    * def executeRequestsForSpecifiedUsers =
    """
  function(totalNoOfUsers){
  for(i=0; i<totalNoOfUsers; i++){
  karate.log('Run test round: '+(i+1));
  karate.call('../featuresreusable/getUserNamesForLoop.feature', { count : i });
  }
  java.lang.Thread.sleep(1*1000);
   }
     """

  Scenario: Execute the loop request. Call the Execute the loop request from getUserNamesForLoop.feature
    * def users = baseData.usernames.length
    * call executeRequestsForSpecifiedUsers users