@ignore
Feature: A test to send requests with multiple PII's and ensure the response status for every request is 200

  Background:
    * def baseData = read('../testdata/multiplepiiinfo.json')
    * def executeRequestsForUsers = read('../testcustomscripts/ExecuteLoop.js')


  Scenario: A scenario to execute multiple requests using different PII's and validate the response status
    * def noOfPIIs = baseData.piiinfo.users.length
    * call executeRequestsForUsers noOfPIIs