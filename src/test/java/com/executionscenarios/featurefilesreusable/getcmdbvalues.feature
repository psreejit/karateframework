@ignore
Feature: A sample feature to read an external value

  Scenario: Read the external value
    Given url 'http://www.mocky.io/v2/5abbea722d000029009bdf7b'
    When method get
    Then status 200
    * def enpointURI = response.endPointURI
    * print enpointURI