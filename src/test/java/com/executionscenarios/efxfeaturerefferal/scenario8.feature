@ignore
Feature: A sample test a json using a data table

  Background:
    * def endPointUrl = caponeurl
    * def baseJsonRequest = read('../requesttemplates/request3capone.json')
    * configure headers = read('../testauthorizations/caponeheaders.json')

  Scenario Outline: A test to match values using scenario outline
    Given url endPointUrl
    And  header Content-Type = 'application/vnd.com.equifax.ic.saas.request.v1+json'
    And request baseJsonRequest
    When method post
    Then status 202
    * def efxUSConsCrdtRpt = $.applicants.primaryConsumer.equifaxUSConsumerCreditReport.equifaxUSConsumerCreditReport
    * print efxUSConsCrdtRpt
    * def scanAlerts = [{ "code": "#string", "description": "#string"}]
    Then match <jsonPath> <expression> <expectedValue>

    Examples:
      | jsonPath                              | expression | expectedValue |
      | efxUSConsCrdtRpt.consumerReferralCode | ==         | '198'         |
      | efxUSConsCrdtRpt.customerNumber       | ==         | '#string'     |
      | efxUSConsCrdtRpt.outputFormatCode     | ==         | '02'          |
      | efxUSConsCrdtRpt.identityScanAlerts   | ==         | '#array'      |
      | efxUSConsCrdtRpt.additionalMultiples  | !=         | '1'           |
      | efxUSConsCrdtRpt.identityScanAlerts   | ==         | scanAlerts    |

  Scenario Outline: A test to assert values using scenario outline
    Given url endPointUrl
    And  header Content-Type = 'application/vnd.com.equifax.ic.saas.request.v1+json'
    And request baseJsonRequest
    When method post
    Then status 202
    * def efxUSConsCrdtRpt = $.applicants.primaryConsumer.equifaxUSConsumerCreditReport.equifaxUSConsumerCreditReport
    * print efxUSConsCrdtRpt
    Then assert <path> <expr> <value>

    Examples:
      | path                                 | expr | value |
      | efxUSConsCrdtRpt.additionalMultiples | >=   | 0     |