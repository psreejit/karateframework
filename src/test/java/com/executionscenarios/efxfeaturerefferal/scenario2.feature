@ignore
Feature: A Sample feature to illustrate JSON webservice test with Data, validations in another pluggable JSON

  Background:
    * def baseJsonRequest = read('../requesttemplates/request2.json')
    * def baseData = read('../testdata/sampledata.json')
    * def endPointUrl = endPointURI + path

  Scenario: SC-1_My Sample Scenario - Creating a request by mapping PII to a request template and validating response using predefined data

    * def piInfo = get[0] baseData.testData.Scenario1.inputPII.specifiedPerson
    * def expectedVerificationValues = baseData.testData.Scenario1.verificationData

    Given url endPointUrl
    And request baseJsonRequest
    * print baseJsonRequest
    * set baseJsonRequest.autoRequest.applicants.applicant
      | path            | value  |
      | specifiedPerson | piInfo |
    * print baseJsonRequest
    When method post
    Then status 200
    * def actualAutoResponseProducts = response.autoResponse.applicationHeader.products
    * print actualAutoResponseProducts
    * print expectedVerificationValues
    And match expectedVerificationValues == actualAutoResponseProducts