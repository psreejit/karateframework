@ignore
Feature: A reusable request to fetch multiple PII from outside send and check response results

  Background:
    * def endPointUrl = caponeurl
    * def baseJsonRequest = read('../requesttemplates/request4caponemultiplepii.json')
    * def testData = read('../testdata/multiplepiiinfo.json')
    * configure headers = read('../testauthorizations/caponeheaders.json')
    * def pIICount = count

  Scenario: A scenario to execute multiple requests using different PII's and validate the response
    Given url endPointUrl
    And  header Content-Type = 'application/vnd.com.equifax.ic.saas.request.v1+json'
    And request baseJsonRequest
    * def testDataValues = testData.piiinfo.users[pIICount]
    * set baseJsonRequest.applicants.primaryConsumer.personalInformation
      | path                    | value                     |
      | name[*].firstName       | testDataValues.firstName  |
      | name[*].lastName        | testDataValues.lastName   |
      | socialSecurityNumber    | testDataValues.ssn        |
      | addresses[*].streetName | testDataValues.streetName |
      | addresses[*].streetType | testDataValues.streetType |
      | addresses[*].city       | testDataValues.city       |
      | addresses[*].state      | testDataValues.state      |
      | addresses[*].zip        | testDataValues.zip        |


    * print baseJsonRequest